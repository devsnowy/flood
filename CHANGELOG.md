## [1.0.0] (April 21, 2017)
* First "official" release
* Change log and semver versioning (finally)
* Control basic rTorrent settings via web UI
  * Transfer rate limiting
  * Connection settings
  * Resource utilization
* Add torrents via URLs or files
* User authentication
* UI translations (only en, fr, and nl)
* Custom torrent tags
* Customizable torrent list
  * "Expanded" and "condensed" views
  * Customizable torrent detail columns
* Basic torrent list filtering (by status, tag, and tracker)
* Auto-download torrents from RSS feeds
